package com.blueentity.skycross.ui.auth.signin

import SignInFormState
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blueentity.skycross.R
import com.blueentity.skycross.data.dao.UserDao
import com.blueentity.skycross.data.generic.Event
import com.blueentity.skycross.data.generic.Result
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class SignInViewModel(private val mAuth: FirebaseAuth, private val db: FirebaseFirestore): ViewModel() {

    private val _loginForm = MutableLiveData<SignInFormState>()
    val signInFormState: LiveData<SignInFormState> = _loginForm

    private val _loginResult = MutableLiveData<Event<Result<UserDao>>>()
    val loginResult: LiveData<Event<Result<UserDao>>> = this._loginResult

    fun login(email: String, password: String) {
        mAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener {
            if(it.user!!.isEmailVerified) {
                db.collection("users").document(it.user!!.uid).get()
                    .addOnSuccessListener { snapshot ->
                        val user = snapshot.toObject(UserDao::class.java)
                        _loginResult.value = Event(Result.Success(user!!))
                    }.addOnFailureListener { e ->
                        _loginResult.value = Event(Result.Error(e))
                    }
            } else {
                _loginResult.value = Event(Result.Error(Exception("Please verify your account to continue")))
                mAuth.signOut()
            }
        }.addOnFailureListener {
            Log.e("login",it.stackTrace.toString(), it)
            _loginResult.value = Event(Result.Error(it))
        }
    }

    fun formDataChanged(email: String, password: String) {
        if(email.isEmpty()) {
            _loginForm.value = SignInFormState(emailError = R.string.error_empty_field)
        } else if(!isEmailValid(email)) {
            _loginForm.value = SignInFormState(emailError = R.string.error_invalid_email)
        }

        if(password.isEmpty()) {
            _loginForm.value = SignInFormState(passwordError = R.string.error_empty_field)
        } else if(!isPasswordValid(password)) {
            _loginForm.value = SignInFormState(passwordError = R.string.error_invalid_password)
        }

        if(isEmailValid(email) && isPasswordValid(password)) {
            _loginForm.value = SignInFormState(isDataValid = true)
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.isNotEmpty() && password.trim { it <= ' ' }.length >= 8
    }
}