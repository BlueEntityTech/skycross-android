package com.blueentity.skycross.ui.main.list

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.blueentity.skycross.R
import com.blueentity.skycross.data.enums.DialogType
import com.blueentity.skycross.data.model.Product
import com.blueentity.skycross.ui.dialogs.ListItemEditDialog
import com.blueentity.skycross.ui.dialogs.ProductInfoDialog
import com.blueentity.skycross.ui.main.MainFragmentDirections
import com.blueentity.skycross.ui.user.UserViewModel
import com.blueentity.skycross.ui.user.UserViewModelFactory
import kotlinx.android.synthetic.main.dialog_edit_list_item.*
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment(),
    ProductsRecyclerViewAdapter.ProductsRecyclerViewCallback,
    ListItemEditDialog.ListItemEditDialogCallback, ProductInfoDialog.ProductInfoDialogCallback {

    private var navController: NavController? = null
    private lateinit var viewModel: ListViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, ListViewModelFactory())
            .get(ListViewModel::class.java)

        activity?.let {
            userViewModel = ViewModelProvider(this, UserViewModelFactory())
                .get(UserViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(requireActivity().findViewById(R.id.nav_host))

        initButtons()
        if(userViewModel.isLoggedIn()) {
            initProductsList()
            label_empty.text = getString(R.string.label_empty_list)

        } else {
            group_empty.visibility = View.VISIBLE
            label_empty.text = getString(R.string.label_signed_out)
        }
    }

    private fun initButtons() {
        button_find_pharmacy.setOnClickListener {
            Toast.makeText(context, "Not available in the prototype", Toast.LENGTH_SHORT).show()
        }
        button_search.setOnClickListener {
            navController!!.navigate(MainFragmentDirections.actionMainFragmentToSearchFragment())
        }
    }

    private fun initProductsList() {
        val adapter = ProductsRecyclerViewAdapter(
            ArrayList<Pair<Product, Double>>().toTypedArray(), requireContext(), this)

        products.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

        viewModel.dataset.observe(viewLifecycleOwner, Observer { items ->
            adapter.dataset = items
            adapter.notifyDataSetChanged()

            products.visibility = if(items.isEmpty()) View.GONE else View.VISIBLE
            group_empty.visibility = if(items.isEmpty()) View.VISIBLE else View.GONE
        })
    }

    override fun onItemClicked(position: Int) {
        ProductInfoDialog(
            Dialog(requireContext()),
            viewModel.getItem(position).first,
            DialogType.LISTITEM,
            position,
            this
        ).show()
    }

    override fun onEditButtonClicked(position: Int) {
        ListItemEditDialog(
            Dialog(requireContext()),
            viewModel.getItem(position),
            position,
            this
        ).show()
    }

    override fun onSaveButtonClicked(item: Pair<Product, Double>, position: Int) {
        viewModel.updateQuantity(editText_quantity.text.toString().toDouble(), position) }

    override fun onDeleteButtonClicked(position: Int) {
        viewModel.removeItem(position)
    }

    override fun onAddButtonClicked(product: Product) {
        //ignore
    }

    override fun onFindButtonClicked() {
        Toast.makeText(context, "Not available in the prototype", Toast.LENGTH_SHORT).show()
    }

}