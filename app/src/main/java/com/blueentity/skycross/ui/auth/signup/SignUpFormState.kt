package com.blueentity.skycross.ui.auth.signup

data class SignUpFormState (
            val emailError: Int? = null,
            val firstNameError: Int? = null,
            val lastNameError: Int? = null,
            val passwordError: Int? = null,
            val confirmPasswordError: Int? = null,
            val isDataValid: Boolean = false)
