package com.blueentity.skycross.ui.dialogs

import android.app.Dialog
import com.blueentity.skycross.R
import com.blueentity.skycross.data.model.Product
import kotlinx.android.synthetic.main.dialog_edit_list_item.*

class ListItemEditDialog(private val dialog: Dialog,
                         private var item: Pair<Product, Double>,
                         private val position: Int,
                         private val callback: ListItemEditDialogCallback
) {

    interface ListItemEditDialogCallback {
        fun onSaveButtonClicked(item: Pair<Product, Double>, position: Int)
        fun onDeleteButtonClicked(position: Int)
    }

    init {
        initDialog()
    }

    private fun initDialog() {
        dialog.setContentView(R.layout.dialog_edit_list_item)

        updateDialogInfo()

        dialog.editText_quantity.setText(item.second.toString())

        dialog.button_save.setOnClickListener {
            callback.onSaveButtonClicked(item.copy(
                second = dialog.editText_quantity.text.toString().toDouble()), position)
            dialog.dismiss()
        }
        dialog.button_delete.setOnClickListener {
            callback.onDeleteButtonClicked(position)
            dialog.dismiss()
        }
    }

    private fun updateDialogInfo() {
        when(item.first) {
            is Product.NonDrug -> {
                val data: Product.NonDrug = item.first as Product.NonDrug

                dialog.apply {
                    title_name.text = data.name
                    subtitle_generic_name.text = getTags(data.tags)
                    value_packaging.text = data.packaging
                    value_contents.text = context.getString(
                        R.string.var_contents,
                        data.quantity.toString(),
                        data.unit)
                    body_desc.text = data.desc
                }
            }
            is Product.Branded -> {
                val data: Product.Branded = item.first as Product.Branded

                dialog.apply {
                    title_name.text = context.getString(
                        R.string.var_title_list_item_branded,
                        data.name,
                        data.dosage)
                    subtitle_generic_name.text = data.generic!!.name
                    value_packaging.text = data.packaging
                    value_contents.text = context.getString(
                        R.string.var_contents,
                        data.quantity.toString(),
                        data.unit)
                    body_desc.text = data.desc
                }
            }
        }
    }

    private fun getTags(tags: ArrayList<String>): String {
        var tagString = ""
        for(index in tags.indices) {
            tagString += tags[index]
            tagString += if(index < tags.size - 1) ", " else ""
        }

        return tagString
    }

    fun show() {
        dialog.show()
    }
}