package com.blueentity.skycross.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blueentity.skycross.data.dao.products.DrugDao
import com.blueentity.skycross.data.dao.products.GenericDao
import com.blueentity.skycross.data.dao.products.ProductDao
import com.blueentity.skycross.data.model.Product
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class SearchViewModel(private val db: FirebaseFirestore) : ViewModel() {

    private val names = ArrayList<Product>()

    private val _names = MutableLiveData<Array<Product>>()
    val resultNames: LiveData<Array<Product>> = _names

    private val _generics = MutableLiveData<Array<Product>>()
    val resultGenerics: LiveData<Array<Product>> = _generics

    private val _tags = MutableLiveData<Array<Product>>()
    val resultTags: LiveData<Array<Product>> = _tags

    init {
        clearResults()
    }

    private fun clearResults() {
        names.clear()
        _names.value = ArrayList<Product>().toTypedArray()
        _generics.value = ArrayList<Product.Branded>().toTypedArray()
        _tags.value = ArrayList<Product.NonDrug>().toTypedArray()
    }

    fun search(query: String?) {
        clearResults()
        if (!query.isNullOrEmpty()) {
            fetchDrugsFromRemote(query)
            fetchProductsFromRemote(query)
            fetchGenericsFromRemote(query)
            fetchTagsFromRemote(query)
        }
    }

    private fun fetchDrugsFromRemote(query: String) {
        db.collection("drugs").whereEqualTo("name", query).get()
            .addOnSuccessListener { snapshot ->
                if(snapshot.documents.isNotEmpty()) {
                    for(doc in snapshot) {
                        var product = doc.toObject(DrugDao::class.java).toProduct()
                        product.genericRef.get()
                            .addOnSuccessListener { document ->
                                product = product.withGeneric(
                                    document.toObject(GenericDao::class.java)!!.toProduct())

                                _names.value = names.apply {
                                    add(product)
                                }.toTypedArray()
                            }
                    }
                }
            }
    }

    private fun fetchProductsFromRemote(query: String) {
        db.collection("products").whereEqualTo("name", query).get()
            .addOnSuccessListener { snapshot ->
                if(snapshot.documents.isNotEmpty()) {
                    for(doc in snapshot) {
                        names.add(doc.toObject(ProductDao::class.java).toProduct())
                    }
                    _names.value = names.toTypedArray()
                }
            }
    }

    private fun fetchGenericsFromRemote(query: String) {
        db.collection("generics").whereEqualTo("name", query).get()
            .addOnSuccessListener {  snapshot ->
                if(snapshot.documents.isNotEmpty()) {
                    for(doc in snapshot) {
                        val ref = db.collection("generics").document(doc.id)
                        fetchDrugsFromGenericResult(ref)
                    }
                }
            }
    }

    private fun fetchDrugsFromGenericResult(ref: DocumentReference) {
        db.collection("drugs").whereEqualTo("genericRef", ref).get()
            .addOnSuccessListener { snapshot ->
                if(snapshot.documents.isNotEmpty()) {
                    for (doc in snapshot) {
                        var product = doc.toObject(DrugDao::class.java).toProduct()
                        product.genericRef.get()
                            .addOnSuccessListener { document ->
                                product = product.withGeneric(
                                    document.toObject(GenericDao::class.java)!!.toProduct())

                                _generics.value = ArrayList<Product>().apply {
                                    addAll(_generics.value as Array<out Product>)
                                    add(product)
                                }.toTypedArray()
                            }

                    }
                }
            }
    }

    private fun fetchTagsFromRemote(query: String) {
        db.collection("products")
            .whereArrayContainsAny("tags", query.split(" ")).get()
            .addOnSuccessListener { snapshot ->
                if(snapshot.documents.isNotEmpty()) {
                    _tags.value = ArrayList<Product>().apply {
                        for (doc in snapshot) {
                            add(doc.toObject(ProductDao::class.java).toProduct())
                        }
                    }.toTypedArray()
                }
            }
    }
}
