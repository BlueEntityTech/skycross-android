package com.blueentity.skycross.ui.search

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.blueentity.skycross.R
import com.blueentity.skycross.data.enums.DialogType
import com.blueentity.skycross.data.model.Product
import com.blueentity.skycross.ui.dialogs.ProductInfoDialog
import com.blueentity.skycross.ui.main.MainActivity
import com.blueentity.skycross.ui.user.UserViewModel
import com.blueentity.skycross.ui.user.UserViewModelFactory
import kotlinx.android.synthetic.main.fragment_search.*


class SearchFragment : Fragment(), ResultsRecyclerViewAdapter.ResultsRecyclerViewCallback,
    ProductInfoDialog.ProductInfoDialogCallback {

    private var navController: NavController? = null
    private lateinit var viewModel: SearchViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, SearchViewModelFactory())
            .get(SearchViewModel::class.java)

        activity?.let {
            userViewModel = ViewModelProvider(this, UserViewModelFactory())
                .get(UserViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(requireActivity().findViewById(R.id.nav_host))

        initToolbar()

        initBrands()
        initGenerics()
        initTags()
    }

    private fun initToolbar() {
        (requireActivity() as MainActivity).apply {
            setSupportActionBar(toolbar)
            supportActionBar!!.apply {
                setDisplayHomeAsUpEnabled(true)
                setHasOptionsMenu(true)
                setDisplayShowTitleEnabled(false)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_search, menu)

        (menu.findItem(R.id.action_search).actionView as SearchView).setOnQueryTextListener(
            object: SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    loading.visibility = View.VISIBLE
                    label_empty.visibility = View.GONE
                    viewModel.search(query)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    loading.visibility = View.VISIBLE
                    label_empty.visibility = View.GONE
                    viewModel.search(newText)
                    return true
                }
            })
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId) {
            R.id.action_search -> {

            }
            else -> {
                navController!!.navigateUp()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initBrands() {
        val adapter = ResultsRecyclerViewAdapter(
            ArrayList<Product>().toTypedArray(), requireContext(), this)

        from_brands.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

        viewModel.resultNames.observe(viewLifecycleOwner, Observer { items ->
            adapter.dataset = items
            adapter.notifyDataSetChanged()

            group_brands.visibility = if(items.isEmpty()) View.GONE else View.VISIBLE
            updateResultStatus()
        })
    }

    private fun initGenerics() {
        val adapter = ResultsRecyclerViewAdapter(
            ArrayList<Product>().toTypedArray(), requireContext(), this)

        from_generics.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

        viewModel.resultGenerics.observe(viewLifecycleOwner, Observer { items ->
            adapter.dataset = items
            adapter.notifyDataSetChanged()

            group_generics.visibility = if(items.isEmpty()) View.GONE else View.VISIBLE
            updateResultStatus()
        })
    }

    private fun initTags() {
        val adapter = ResultsRecyclerViewAdapter(
            ArrayList<Product>().toTypedArray(), requireContext(), this)

        from_tags.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

        viewModel.resultTags.observe(viewLifecycleOwner, Observer { items ->
            adapter.dataset = items
            adapter.notifyDataSetChanged()

            group_tags.visibility = if(items.isEmpty()) View.GONE else View.VISIBLE
            updateResultStatus()
        })
    }

    private fun updateResultStatus() {
        loading.visibility = View.INVISIBLE
        label_empty.visibility =
            if (viewModel.resultNames.value!!.isEmpty()
                && viewModel.resultGenerics.value!!.isEmpty()
                && viewModel.resultTags.value!!.isEmpty()) View.VISIBLE else View.GONE
    }

    override fun onItemClicked(product: Product) {
        ProductInfoDialog(
            Dialog(requireContext()),
            product,
            DialogType.BROWSE,
            -1,
            this
        ).show()
    }

    override fun onAddButtonClicked(product: Product) {
        if(userViewModel.isLoggedIn()) {
            userViewModel.addItemToList(product)
            navController!!.navigateUp()
        } else {
            Toast.makeText(context, getString(R.string.label_signed_out),
                Toast.LENGTH_SHORT).show()
        }
    }

    override fun onFindButtonClicked() {
        Toast.makeText(context, "Not available in the prototype", Toast.LENGTH_SHORT).show()
    }

    override fun onDeleteButtonClicked(position: Int) {
        //ignore
    }
}