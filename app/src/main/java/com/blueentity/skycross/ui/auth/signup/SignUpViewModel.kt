package com.blueentity.skycross.ui.auth.signup

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blueentity.skycross.R
import com.blueentity.skycross.data.dao.UserDao
import com.blueentity.skycross.data.generic.Event
import com.blueentity.skycross.data.generic.Result
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.firestore.FirebaseFirestore

class SignUpViewModel(private val mAuth: FirebaseAuth,
                      private val db: FirebaseFirestore): ViewModel() {

    private val _signupForm = MutableLiveData<SignUpFormState>()
    val signUpFormState: LiveData<SignUpFormState> = _signupForm

    private val _signupResult = MutableLiveData<Event<Result<Any>>>()
    val signupResult: LiveData<Event<Result<Any>>> = _signupResult

    fun formDataChanged(email: String, firstName: String, lastName: String,
                        password: String, confirmPass: String) {
        if (email.isEmpty()) {
            _signupForm.value = SignUpFormState(emailError = R.string.error_empty_field)
        } else if (!isEmailValid(email)) {
            _signupForm.value = SignUpFormState(emailError = R.string.error_invalid_email)
        }

        if (firstName.isEmpty()) {
            _signupForm.value = SignUpFormState(firstNameError = R.string.error_empty_field)
        }

        if (lastName.isEmpty()) {
            _signupForm.value = SignUpFormState(lastNameError = R.string.error_empty_field)
        }

        if (password.isEmpty()) {
            _signupForm.value = SignUpFormState(passwordError = R.string.error_empty_field)
        } else if (!isPasswordValid(password)) {
            _signupForm.value = SignUpFormState(passwordError = R.string.error_invalid_password)
        }

        if (confirmPass.isEmpty()) {
            _signupForm.value = SignUpFormState(passwordError = R.string.error_empty_field)
        } else if (!isPasswordMatch(password, confirmPass)) {
            _signupForm.value = SignUpFormState(passwordError = R.string.error_invalid_password)
        }

        if (isEmailValid(email) && isPasswordValid(password) && isPasswordMatch(password, confirmPass)) {
            _signupForm.value = SignUpFormState(isDataValid = true)
        }
    }

    fun signUp(email: String, password: String, firstName: String, lastName: String) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                updateUserData(UserDao(it.user!!.uid, email, firstName, lastName, ArrayList()))
            }.addOnFailureListener {
                when(it) {
                    is FirebaseAuthUserCollisionException -> {
                        _signupForm.value = SignUpFormState(emailError = R.string.error_email_used)
                    }
                    is FirebaseAuthInvalidCredentialsException -> {
                        _signupForm.value = SignUpFormState(emailError = R.string.error_bad_format)
                    }
                    else -> {
                        Log.e("signup",it.message, it)
                    }
                }
                _signupResult.value = Event(Result.Error(it))
            }
    }

    private fun updateUserData(user: UserDao) {
        db.collection("users").document(user.id!!).set(user)
            .addOnSuccessListener {
                _signupResult.value = Event(Result.Success(user))
            }.addOnFailureListener {
                _signupResult.value = Event(Result.Error(it))
            }
    }

    fun sendEmailVerification() {
        mAuth.currentUser!!.sendEmailVerification()
        mAuth.signOut()
    }

    private fun isEmailValid(email: String): Boolean {
        return email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.isNotEmpty() && password.trim { it <= ' ' }.length >= 8
    }

    private fun isPasswordMatch(password: String, confirmPassword: String): Boolean {
        return password == confirmPassword
    }
}