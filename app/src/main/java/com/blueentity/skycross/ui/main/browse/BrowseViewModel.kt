package com.blueentity.skycross.ui.main.browse

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blueentity.skycross.data.dao.locations.BranchDao
import com.blueentity.skycross.data.dao.locations.HospitalDao
import com.blueentity.skycross.data.dao.locations.PharmacyDao
import com.blueentity.skycross.data.model.locations.Branch
import com.blueentity.skycross.data.model.locations.Hospital
import com.google.firebase.firestore.FirebaseFirestore

class BrowseViewModel(private val db: FirebaseFirestore) : ViewModel() {

    private val branchesFilled = ArrayList<Branch>()
    private val _branches = MutableLiveData<ArrayList<Branch>>()
    val branches: LiveData<ArrayList<Branch>> = _branches

    private val _hospitals = MutableLiveData<ArrayList<Hospital>>()
    val hospitals: LiveData<ArrayList<Hospital>> = _hospitals

    init {
        fetchBranchesFromRemote()
        fetchHospitalsFromRemote()
    }

    private fun fetchBranchesFromRemote() {
        //TODO: Implement proximity searching
        db.collection("branches").limit(4).get()
            .addOnSuccessListener { documents ->
                for(doc in documents) {
                    fetchBranchDataFromRemote(doc.toObject(BranchDao::class.java).toBranch())
                }
            }
    }

    private fun fetchBranchDataFromRemote(branch: Branch) {
        branch.pharmacyRef.get()
            .addOnSuccessListener { document ->
                _branches.value = branchesFilled.apply {
                    add(branch.withPharmacy(
                        document.toObject(PharmacyDao::class.java)!!.toPharmacy()))
                }
            }
    }

    private fun fetchHospitalsFromRemote() {
        //TODO: Implement proximity searching
        db.collection("hospitals").limit(4).get()
            .addOnSuccessListener { documents ->
                _hospitals.value = ArrayList<Hospital>().apply {
                    for (doc in documents) {
                        add(doc.toObject(HospitalDao::class.java).toHospital())
                    }
                }
            }
    }
}