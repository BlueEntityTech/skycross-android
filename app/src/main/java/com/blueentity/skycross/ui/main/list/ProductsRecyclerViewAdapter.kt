package com.blueentity.skycross.ui.main.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.blueentity.skycross.R
import com.blueentity.skycross.data.model.Product
import kotlinx.android.synthetic.main.listitem_item.view.*

class ProductsRecyclerViewAdapter(var dataset: Array<Pair<Product, Double>>,
                                  private val context: Context,
                                  private val callback: ProductsRecyclerViewCallback)
    : RecyclerView.Adapter<ProductsRecyclerViewAdapter.ItemsViewHolder>() {

    interface ProductsRecyclerViewCallback {
        fun onItemClicked(position: Int)
        fun onEditButtonClicked(position: Int)
    }

    class ItemsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.title_value_name
        val details: TextView = view.subtitle_value_details
        val edit: ImageView = view.button_edit
        val item: CardView = view.item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        return ItemsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.listitem_item, parent, false))
    }

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        when(dataset[position].first) {
            is Product.NonDrug -> {
                val data = dataset[position].first as Product.NonDrug

                holder.name.text = data.name
                holder.details.text = context.getString(
                    R.string.var_subtitle_list_item_quantity,
                    dataset[position].second.toString(), data.packaging)
            }
            is Product.Branded -> {
                val data = dataset[position].first as Product.Branded

                holder.name.text = context.getString(
                    R.string.var_title_list_item_branded,
                    data.name, data.dosage)
                holder.details.text = context.getString(
                    R.string.var_subtitle_list_item_quantity,
                    dataset[position].second.toString(), data.packaging)
            }
        }

        holder.edit.setOnClickListener {
            callback.onEditButtonClicked(position)
        }
        holder.item.setOnClickListener {
            callback.onItemClicked(position)
        }
    }

    override fun getItemCount() = dataset.size
}