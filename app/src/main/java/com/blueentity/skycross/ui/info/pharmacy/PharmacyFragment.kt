package com.blueentity.skycross.ui.info.pharmacy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.blueentity.skycross.R

class PharmacyFragment : Fragment() {

    companion object {
        fun newInstance() = PharmacyFragment()
    }

    private lateinit var viewModel: PharmaciyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pharmacy, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PharmaciyViewModel::class.java)
        // TODO: Use the ViewModel
    }

}