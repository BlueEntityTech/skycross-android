package com.blueentity.skycross.ui.main.prescriptions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.FirebaseFirestore

class PrescriptionsViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PrescriptionsViewModel::class.java)) {
            return PrescriptionsViewModel(FirebaseFirestore.getInstance()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}