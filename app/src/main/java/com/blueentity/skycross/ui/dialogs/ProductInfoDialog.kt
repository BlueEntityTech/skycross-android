package com.blueentity.skycross.ui.dialogs

import android.app.Dialog
import android.view.View
import com.blueentity.skycross.R
import com.blueentity.skycross.data.enums.DialogType
import com.blueentity.skycross.data.model.Product
import kotlinx.android.synthetic.main.dialog_product_info.*

class ProductInfoDialog(private val dialog: Dialog,
                        private var item: Product,
                        private val type: DialogType,
                        private val position: Int,
                        private val callback: ProductInfoDialogCallback) {

    interface ProductInfoDialogCallback {
        fun onAddButtonClicked(product: Product)
        fun onFindButtonClicked()
        fun onDeleteButtonClicked(position: Int)
    }

    init {
        initDialog()
    }

    private fun initDialog() {
        dialog.setContentView(R.layout.dialog_product_info)

        updateDialogInfo()

        dialog.button_add.setOnClickListener {
            callback.onAddButtonClicked(item)
            dialog.dismiss()
        }
        dialog.button_find.setOnClickListener {
            callback.onFindButtonClicked()
            dialog.dismiss()
        }
        dialog.button_delete.setOnClickListener {
            callback.onDeleteButtonClicked(position)
            dialog.dismiss()
        }
    }

    private fun updateDialogInfo() {
        when(item) {
            is Product.NonDrug -> {
                val data: Product.NonDrug = item as Product.NonDrug

                dialog.apply {
                    title_name.text = data.name
                    subtitle_generic_name.text = getTags(data.tags)
                    value_packaging.text = data.packaging
                    value_contents.text = context.getString(
                        R.string.var_contents,
                        data.quantity.toString(),
                        data.unit)
                    body_desc.text = data.desc
                }
            }
            is Product.Branded -> {
                val data: Product.Branded = item as Product.Branded

                dialog.apply {
                    title_name.text = context.getString(
                        R.string.var_title_list_item_branded,
                        data.name,
                        data.dosage)
                    subtitle_generic_name.text = data.generic!!.name
                    value_packaging.text = data.packaging
                    value_contents.text = context.getString(
                        R.string.var_contents,
                        data.quantity.toString(),
                        data.unit)
                    body_desc.text = data.desc
                }
            }
        }

        when(type) {
            DialogType.LISTITEM -> {
                dialog.button_add.visibility = View.GONE
                dialog.space_browse.visibility = View.GONE
            }
            DialogType.BROWSE -> {
                dialog.group_quantity.visibility = View.GONE
                dialog.button_delete.visibility = View.GONE
                dialog.space_listitem.visibility = View.GONE
            }
        }
    }

    private fun getTags(tags: ArrayList<String>): String {
        var tagString = ""
        for(index in tags.indices) {
            tagString += tags[index]
            tagString += if(index < tags.size - 1) ", " else ""
        }

        return tagString
    }

    fun show() {
        dialog.show()
    }
}