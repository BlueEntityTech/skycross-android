package com.blueentity.skycross.ui.main.prescriptions

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

class PrescriptionsFragment : Fragment() {

    private lateinit var viewModel: PrescriptionsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            viewModel = ViewModelProvider(this, PrescriptionsViewModelFactory())
                .get(PrescriptionsViewModel::class.java)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PrescriptionsViewModel::class.java)
        // TODO: Use the ViewModel
    }

}