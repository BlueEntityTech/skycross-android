package com.blueentity.skycross.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blueentity.skycross.data.dao.UserDao
import com.blueentity.skycross.data.dao.products.ItemDao
import com.blueentity.skycross.data.model.Product
import com.blueentity.skycross.data.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class UserViewModel(private val mAuth: FirebaseAuth, private val db: FirebaseFirestore) : ViewModel() {

    private val _user = MutableLiveData<User?>()
    val user: LiveData<User?> = _user

    private val _profilePicture = MutableLiveData<String?>()
    val profilePicture: LiveData<String?> = _profilePicture

    init {
        if(mAuth.currentUser != null) {
            fetchUserDataFromRemote()
        }
    }

    fun isLoggedIn(): Boolean {
        return mAuth.currentUser != null
    }

    fun signOut() {
        mAuth.signOut()
        setUser(null)
    }

    fun setUser(user: User?) {
        _user.value = user
    }

    private fun fetchUserDataFromRemote() {
        db.collection("users").document(mAuth.currentUser!!.uid).get()
            .addOnSuccessListener { document ->
                setUser(document.toObject(UserDao::class.java)!!.toUser())
                _profilePicture.value = user.value?.imageUri
            }
    }

    fun addItemToList(product: Product) {
        db.collection("users").document(mAuth.currentUser!!.uid).update("items",
            user.value!!.items.apply {
                add(ItemDao(
                    when(product) {
                        is Product.Branded ->
                            db.collection("drugs").document(product.id)
                        is Product.NonDrug ->
                            db.collection("products").document(product.id)
                    }
                    , 1.0))
            })
    }
}