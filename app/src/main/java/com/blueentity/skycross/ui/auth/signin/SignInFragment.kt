package com.blueentity.skycross.ui.auth.signin

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.blueentity.skycross.R
import com.blueentity.skycross.data.generic.Result
import com.blueentity.skycross.ui.user.UserViewModel
import com.blueentity.skycross.ui.user.UserViewModelFactory
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import kotlinx.android.synthetic.main.fragment_sign_in.view.*

class SignInFragment : Fragment(), View.OnClickListener {

    private var navController: NavController? = null

    private lateinit var viewModel: SignInViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, SignInViewModelFactory())
            .get(SignInViewModel::class.java)
        requireActivity().let {
            userViewModel = ViewModelProvider(this, UserViewModelFactory())
                .get(UserViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        view.link_sign_up.setOnClickListener(this)
        val email = view.editText_email
        val password = view.editText_password
        val signInBtn = view.button_signIn
        val loading = view.loading

        viewModel.signInFormState.observe(viewLifecycleOwner, Observer {
            val loginState = it ?: return@Observer

            signInBtn.isEnabled = loginState.isDataValid

            if(loginState.emailError != null) {
                email.error = getString(loginState.emailError)
            }
            if(loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        viewModel.loginResult.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let {

                signInBtn.isEnabled = true
                signInBtn.text = getString(R.string.action_sign_in)
                loading.visibility = View.GONE

                when(it) {
                    is Result.Error -> showLoginFailed(it.exception)
                    is Result.Success -> {
                        userViewModel.setUser(it.data.toUser())
                        navController!!.navigate(SignInFragmentDirections.actionSignInFragmentToNavGraphRoot())
                    }
                }
            }
        })

        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                viewModel.formDataChanged(
                    email.text.toString(),
                    password.text.toString()
                )
            }
        }
        email.addTextChangedListener(afterTextChangedListener)
        password.addTextChangedListener(afterTextChangedListener)

        signInBtn.setOnClickListener {
            loading.visibility = View.VISIBLE
            signInBtn.isEnabled = false
            signInBtn.text = ""

            viewModel.login(email.text.toString(), password.text.toString())
        }
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
//            R.id.button_google_signin -> {
//                //TODO: Add google sign-in
//            }
            R.id.link_sign_up -> {
                navController!!.navigate(R.id.action_signInFragment_to_signUpFragment)
            }
        }
    }

    private fun showLoginFailed(e: Exception) {
        val errorMessage: String? = when(e) {
            is FirebaseAuthInvalidUserException -> getString(R.string.error_invalid_credentials)
            is FirebaseNetworkException -> getString(R.string.error_network_error)
            else -> e.message
        }
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }
}