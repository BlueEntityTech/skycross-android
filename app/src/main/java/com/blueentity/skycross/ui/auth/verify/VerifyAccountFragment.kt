package com.blueentity.skycross.ui.auth.verify

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.blueentity.skycross.R
import kotlinx.android.synthetic.main.fragment_verify_account.view.*

class VerifyAccountFragment : Fragment(), View.OnClickListener {

    private var navController: NavController? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_verify_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
        view.button_continue.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.button_continue -> {
                navController!!.navigate(
                    VerifyAccountFragmentDirections.actionVerifyAccountFragmentToSignInFragment())
            }
        }
    }
}