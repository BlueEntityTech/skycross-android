package com.blueentity.skycross.ui.main.browse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.blueentity.skycross.R
import com.blueentity.skycross.data.enums.LocationType
import com.blueentity.skycross.data.model.locations.Branch
import com.blueentity.skycross.data.model.locations.Hospital
import com.blueentity.skycross.ui.main.MainFragmentDirections
import com.blueentity.skycross.ui.user.UserViewModel
import com.blueentity.skycross.ui.user.UserViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_browse.*

class BrowseFragment : Fragment(),
    LocationsRecyclerViewAdapter.LocationsRecyclerViewAdapterCallback {

    private var navController: NavController? = null

    private lateinit var viewModel: BrowseViewModel
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.let {
            viewModel = ViewModelProvider(this, BrowseViewModelFactory())
                .get(BrowseViewModel::class.java)

            userViewModel = ViewModelProvider(this, UserViewModelFactory())
                .get(UserViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_browse, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(requireActivity().findViewById(R.id.nav_host))

        userViewModel.profilePicture.observe(viewLifecycleOwner, Observer { data ->
            Picasso.get()
                .load(data)
                .fit()
                .centerCrop()
                .into(profile_picture)
        })

        button_search.setOnClickListener {
            navController!!.navigate(MainFragmentDirections.actionMainFragmentToSearchFragment())
        }
        profile_picture.setOnClickListener {
            navController!!.navigate(MainFragmentDirections.actionMainFragmentToUserProfileFragment())
        }

        initBranchList()
        initHospitalList()
    }

    private fun initBranchList() {
        val adapter = LocationsRecyclerViewAdapter(
            ArrayList<Branch>().toTypedArray(), LocationType.BRANCH, requireContext(),this)

        pharmacies.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

        viewModel.branches.observe(viewLifecycleOwner, Observer { branches ->
            adapter.dataset = branches.toTypedArray()
            adapter.notifyDataSetChanged()
        })
    }

    private fun initHospitalList() {
        val adapter = LocationsRecyclerViewAdapter(
            ArrayList<Hospital>().toTypedArray(), LocationType.HOSPITAL, requireContext(), this)

        hospitals.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

        viewModel.hospitals.observe(viewLifecycleOwner, Observer { hospitals ->
            adapter.dataset = hospitals.toTypedArray()
            adapter.notifyDataSetChanged()
        })
    }

    override fun onItemClicked(position: Int, type: LocationType) {
        when(type) {
            LocationType.BRANCH -> navController!!.navigate(
                MainFragmentDirections.actionMainFragmentToBranchFragment())
            LocationType.PHARMACY -> navController!!.navigate(
                MainFragmentDirections.actionMainFragmentToPharmacyFragment())
            LocationType.HOSPITAL -> navController!!.navigate(
                MainFragmentDirections.actionMainFragmentToHospitalFragment())
        }
    }
}