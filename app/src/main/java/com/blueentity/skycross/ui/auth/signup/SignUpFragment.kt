package com.blueentity.skycross.ui.auth.signup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.blueentity.skycross.R
import com.blueentity.skycross.data.generic.Result
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import kotlinx.android.synthetic.main.fragment_sign_up.view.*

class SignUpFragment : Fragment(), View.OnClickListener {

    private var navController: NavController? = null
    private lateinit var viewModel: SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, SignUpViewModelFactory())
            .get(SignUpViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        view.link_sign_in.setOnClickListener(this)
        val email = view.editText_email
        val firstName = view.editText_first_name
        val lastName = view.editText_last_name
        val password = view.editText_password
        val confirmPassword = view.editText_confirm_password
        val loading = view.loading
        val signUpBtn = view.button_signup.apply {
            setOnClickListener {
                loading.visibility = View.VISIBLE
                isEnabled = false
                text = ""
                viewModel.signUp(
                    email.text.toString(),
                    password.text.toString(),
                    firstName.text.toString(),
                    lastName.text.toString())
            }
        }

        viewModel.signUpFormState.observe(viewLifecycleOwner, Observer {
            val signUpFormState = it ?: return@Observer

            signUpBtn.isEnabled = signUpFormState.isDataValid

            if(signUpFormState.emailError != null) {
                email.error = getString(signUpFormState.emailError)
            }
            if(signUpFormState.firstNameError != null) {
                firstName.error = getString(signUpFormState.firstNameError)
            }
            if(signUpFormState.lastNameError != null) {
                lastName.error = getString(signUpFormState.lastNameError)
            }
            if(signUpFormState.passwordError != null) {
                password.error = getString(signUpFormState.passwordError)
            }
            if(signUpFormState.confirmPasswordError != null) {
                confirmPassword.error = getString(signUpFormState.confirmPasswordError)
            }
        })

        viewModel.signupResult.observe(viewLifecycleOwner, Observer { it ->
            it.getContentIfNotHandled()?.let {
                loading.visibility = View.GONE
                when(it) {
                    is Result.Success -> {
                        viewModel.sendEmailVerification()
                        navController!!.navigate(
                            SignUpFragmentDirections.actionSignUpFragmentToVerifyAccountFragment())
                    }
                    is Result.Error ->  {
                        signUpBtn.text = getString(R.string.action_sign_up)

                        when(it.exception) {
                            is FirebaseAuthUserCollisionException -> signUpBtn.isEnabled = false
                            is FirebaseAuthInvalidCredentialsException -> signUpBtn.isEnabled = false
                            else -> {
                                signUpBtn.isEnabled = true
                                showSignUpFailed(it.exception)
                            }
                        }
                    }
                }
            }
        })

        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                viewModel.formDataChanged(
                    email.text.toString(),
                    firstName.text.toString(),
                    lastName.text.toString(),
                    password.text.toString(),
                    confirmPassword.text.toString()
                )
            }
        }
        email.addTextChangedListener(afterTextChangedListener)
        firstName.addTextChangedListener(afterTextChangedListener)
        lastName.addTextChangedListener(afterTextChangedListener)
        password.addTextChangedListener(afterTextChangedListener)
        confirmPassword.addTextChangedListener(afterTextChangedListener)
    }

    private fun showSignUpFailed(e: Exception) {
        val errorMessage: String? = when(e) {
            is FirebaseNetworkException -> {
                getString(R.string.error_network_error)
            }
            else -> {
                e.message
            }
        }
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.link_sign_in -> {
                navController!!.navigateUp()
            }
        }
    }
}