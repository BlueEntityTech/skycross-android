package com.blueentity.skycross.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blueentity.skycross.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}