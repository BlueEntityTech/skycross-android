package com.blueentity.skycross.ui.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.blueentity.skycross.R
import com.blueentity.skycross.data.model.Product
import kotlinx.android.synthetic.main.listitem_result.view.*

class ResultsRecyclerViewAdapter(var dataset: Array<Product>,
                                 private val context: Context,
                                 private val callback: ResultsRecyclerViewCallback)
    : RecyclerView.Adapter<ResultsRecyclerViewAdapter.ResultsViewHolder>() {

    interface ResultsRecyclerViewCallback {
        fun onItemClicked(product: Product)
    }

    class ResultsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.title_value_name
        val details: TextView = view.subtitle_value_details
        val item: ConstraintLayout = view.item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsViewHolder {
        return ResultsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.listitem_result, parent, false))
    }

    override fun onBindViewHolder(holder: ResultsViewHolder, position: Int) {
        when(dataset[position]) {
            is Product.NonDrug -> {
                val data = dataset[position] as Product.NonDrug

                holder.name.text = data.name
                holder.details.text = context.getString(
                    R.string.var_contents,
                    data.quantity.toString(),
                    data.unit)
            }
            is Product.Branded -> {
                val data = dataset[position] as Product.Branded

                holder.name.text = context.getString(
                    R.string.var_title_list_item_branded,
                    data.name, data.dosage)
                holder.details.text = context.getString(
                    R.string.var_contents,
                    data.quantity.toString(),
                    data.unit)
            }
        }

        holder.item.setOnClickListener {
            callback.onItemClicked(dataset[position])
        }
    }

    override fun getItemCount() = dataset.size
}