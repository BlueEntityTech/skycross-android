package com.blueentity.skycross.ui.main.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.blueentity.skycross.data.dao.UserDao
import com.blueentity.skycross.data.dao.products.DrugDao
import com.blueentity.skycross.data.dao.products.GenericDao
import com.blueentity.skycross.data.dao.products.ItemDao
import com.blueentity.skycross.data.dao.products.ProductDao
import com.blueentity.skycross.data.model.Generic
import com.blueentity.skycross.data.model.Product
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore

class ListViewModel(private val mAuth: FirebaseAuth,
                    private val db: FirebaseFirestore) : ViewModel() {

    private val datasetFilled = ArrayList<Pair<Product, Double>>()
    private val _dataset = MutableLiveData<Array<Pair<Product, Double>>>()
    val dataset: LiveData<Array<Pair<Product, Double>>> = _dataset

    init {
        _dataset.value = ArrayList<Pair<Product, Double>>().toTypedArray()
        trackListFromRemote()
    }

    private fun writeChangesToRemote() {
        val data: ArrayList<ItemDao> = ArrayList()

        for(item in datasetFilled) {
            when(item.first) {
                is Product.NonDrug -> {
                    val docRef: DocumentReference = db.collection("products")
                        .document((item.first as Product.NonDrug).id)
                    data.add(ItemDao(docRef, item.second))
                }
                is Product.Branded -> {
                    val docRef: DocumentReference = db.collection("drugs")
                        .document((item.first as Product.Branded).id)
                    data.add(ItemDao(docRef, item.second))
                }
            }
        }

        db.collection("users").document(mAuth.currentUser!!.uid)
            .update("items", data)
    }

    private fun trackListFromRemote() {
        if(mAuth.currentUser != null) {
            db.collection("users").document(mAuth.currentUser!!.uid)
                .addSnapshotListener { snapshot, _ ->
                    if (snapshot != null && snapshot.exists()) {
                        datasetFilled.clear()
                        val user = snapshot.toObject(UserDao::class.java)
                        for (item in user!!.items!!) {
                            getItemData(item)
                        }
                    }
                }
        }
    }

    private fun getItemData(item: ItemDao) {
        item.itemRef!!.get().addOnSuccessListener { document ->
            if(item.itemRef.path.contains("products", ignoreCase = true)) {
                val product =  document.toObject(ProductDao::class.java)

                addItem(Pair<Product, Double>(product!!.toProduct(), item.quantity!!))
            } else {
                val product = document.toObject(DrugDao::class.java)
                getGenericData(product!!.toProduct(), item.quantity!!)
            }
        }
    }

    private fun getGenericData(item: Product.Branded, quantity: Double) {
        item.genericRef.get()
            .addOnSuccessListener { document ->
                val generic: Generic = document
                    .toObject(GenericDao::class.java)!!.toProduct()

                addItem(Pair<Product, Double>(item.withGeneric(generic), quantity))
            }
    }

    fun addItem(item: Pair<Product, Double>) {
        datasetFilled.add(item)
        _dataset.value = datasetFilled.toTypedArray()
    }

    fun getItem(position: Int) : Pair<Product, Double> {
        return datasetFilled[position]
    }

    fun updateQuantity(quantity: Double, position: Int) {
        datasetFilled[position] = datasetFilled[position].copy(second = quantity)
        _dataset.value = datasetFilled.toTypedArray()
        writeChangesToRemote()
    }

    fun removeItem(position: Int) {
        datasetFilled.removeAt(position)
        _dataset.value = datasetFilled.toTypedArray()
        writeChangesToRemote()
    }
}