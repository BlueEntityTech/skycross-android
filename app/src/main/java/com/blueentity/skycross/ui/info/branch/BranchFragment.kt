package com.blueentity.skycross.ui.info.branch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.blueentity.skycross.R

class BranchFragment : Fragment() {

    companion object {
        fun newInstance() = BranchFragment()
    }

    private lateinit var viewModel: BranchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.branch_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BranchViewModel::class.java)
        // TODO: Use the ViewModel
    }

}