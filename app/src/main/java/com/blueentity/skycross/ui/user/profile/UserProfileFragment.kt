package com.blueentity.skycross.ui.user.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.blueentity.skycross.R
import com.blueentity.skycross.ui.user.UserViewModel
import com.blueentity.skycross.ui.user.UserViewModelFactory
import kotlinx.android.synthetic.main.fragment_user_profile.*

class UserProfileFragment : Fragment() {

    private var navController: NavController? = null
    private lateinit var viewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, UserViewModelFactory())
            .get(UserViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        initButtonSignInOut()

        button_sign_in_out.setOnClickListener {
            if(viewModel.isLoggedIn()) {
                viewModel.signOut()
                initButtonSignInOut()
            } else {
                navController!!.navigate(UserProfileFragmentDirections
                    .actionUserProfileFragmentToNavGraphAuth())
            }
        }
    }

    private fun initButtonSignInOut() {
        button_sign_in_out.text =
            if(viewModel.isLoggedIn()) getString(R.string.action_sign_out)
            else getString(R.string.action_sign_in)
    }
}