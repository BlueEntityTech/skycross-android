package com.blueentity.skycross.ui.main.browse

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.blueentity.skycross.R
import com.blueentity.skycross.data.enums.LocationType
import com.blueentity.skycross.data.model.locations.Branch
import com.blueentity.skycross.data.model.locations.Hospital
import com.blueentity.skycross.data.model.locations.Pharmacy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.listitem_location.view.*

class LocationsRecyclerViewAdapter(var dataset: Array<Any>,
                                   var type: LocationType,
                                   private val context: Context,
                                   private val callback: LocationsRecyclerViewAdapterCallback)
    : RecyclerView.Adapter<LocationsRecyclerViewAdapter.LocationsViewHolder>() {

    interface LocationsRecyclerViewAdapterCallback {
        fun onItemClicked(position: Int, type: LocationType)
    }

    class LocationsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.title_name
        val status: TextView = view.subtitle_status
        val item: CardView = view.item
        val image: ImageView = view.image
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationsViewHolder {
        return LocationsViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.listitem_location, parent, false)
        )
    }

    override fun onBindViewHolder(holder: LocationsViewHolder, position: Int) {
        holder.apply {
            when(type) {
                LocationType.BRANCH ->
                    updateUI(holder, dataset[position] as Branch)
                LocationType.PHARMACY ->
                    updateUI(holder, dataset[position] as Pharmacy)
                LocationType.HOSPITAL ->
                    updateUI(holder, dataset[position] as Hospital)
            }

            item.setOnClickListener {
                callback.onItemClicked(position, type)
            }
        }
    }

    private fun updateUI(holder: LocationsViewHolder, data: Branch) {
        if(data.imageUri.isNotEmpty()) {
            Picasso.get()
                .load(data.imageUri)
                .fit()
                .centerCrop()
                .into(holder.image)
        }

        holder.name.text = context.getString(
            R.string.var_title_location_item, data.pharmacy!!.name,
            data.name)

        holder.status.setTextColor(
            if (data.isOpen) context.getColor(R.color.colorPrimary)
            else context.getColor(R.color.textColorSecondary)
        )

        //hardcoded due to prototype restrictions
        holder.status.text = context.getString(R.string.var_subtitle_list_item_location_status,
            if (data.isOpen) context.getString(R.string.label_open)
            else context.getString(R.string.label_closed),
            "0.2 km away")

    }

    private fun updateUI(holder: LocationsViewHolder, data: Pharmacy) {
        //TODO: Implement Pharmacy when it actually becomes a location and not an entity
    }

    private fun updateUI(holder: LocationsViewHolder, data: Hospital) {
        if(data.imageUri.isNotEmpty()) {
            Picasso.get()
                .load(data.imageUri)
                .fit()
                .centerCrop()
                .into(holder.image)
        }

        holder.name.text = data.name

        //hardcoded due to prototype restrictions
        holder.status.setTextColor(
            if (data.hasCapacity) context.getColor(R.color.colorPrimary)
            else context.getColor(R.color.textColorSecondary)
        )
        holder.status.text = context.getString(R.string.var_subtitle_list_item_location_status,
            if (data.hasCapacity) context.getString(R.string.label_not_full)
            else context.getString(R.string.label_full),
            "0.2 km away")

    }

    override fun getItemCount() = dataset.size
}