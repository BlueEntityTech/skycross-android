package com.blueentity.skycross.data.dao.locations

import com.blueentity.skycross.data.model.locations.Hospital
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.GeoPoint

data class HospitalDao(@Exclude
                       @DocumentId
                       val id: String? = null,
                       val name: String? = null,
                       val address: Map<String, String>? = null,
                       val location: GeoPoint? = null,
                       val imageUri: String? = null,
                       val hasCapacity: Boolean? = null) {

    fun toHospital(): Hospital {
        return Hospital(
            id = id!!,
            name = name!!,
            address = address!!,
            location = location!!,
            imageUri = imageUri!!,
            hasCapacity = hasCapacity!!
        )
    }
}