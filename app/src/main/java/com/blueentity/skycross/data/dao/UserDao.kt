package com.blueentity.skycross.data.dao

import com.blueentity.skycross.data.dao.products.ItemDao
import com.blueentity.skycross.data.model.User
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class UserDao(@Exclude
                   @DocumentId
                   val id: String? = null,
                   val email: String? = null,
                   val firstName: String? = null,
                   val lastName: String? = null,
                   val items: ArrayList<ItemDao>? = null,
                   val imageUri: String? = null) {

    fun toUser() : User {
        return User(
            id = id!!,
            email = email!!,
            firstName = firstName!!,
            lastName = lastName!!,
            items = items!!,
            imageUri = imageUri
        )
    }
}
