package com.blueentity.skycross.data.dao.products

import com.google.firebase.firestore.DocumentReference

data class ItemDao(
    val itemRef: DocumentReference? = null,
    var quantity: Double? = null
)