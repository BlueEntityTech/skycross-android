package com.blueentity.skycross.data.model.locations

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import java.io.Serializable

data class Hospital(val id: String,
                    val name: String,
                    val address: Map<String, String>,
                    val location: GeoPoint,
                    val imageUri: String,
                    val hasCapacity: Boolean) {
//    fun serialize(): SerializableHospital {
//        return SerializableHospital(
//            id = id,
//            name = name,
//            desc = desc,
//            sellerRef = sellerRef?.path,
//            defaultImgUri = defaultImgUri,
//            imgUris = if (imgUris != null) imgUris.clone() as ArrayList<String> else null,
//            rates = rates,
//            hasStock = hasStock,
//            tags = if (tags != null) tags.clone() as ArrayList<String> else null
//        )
//    }
}

//data class SerializableProduct(
//    @Exclude
//    @DocumentId
//    val id: String? = null,
//    val name: String? = null,
//    val address: Map<String, String>? = null,
//    val location: GeoPoint? = null,
//    val imageUri: String? = null,
//    val hasCapacity: Boolean? = null
//) : Serializable {
//    fun deserialize() : Product {
//        return Product(
//            id = id,
//            name = name,
//            desc = desc,
//            sellerRef = if (sellerRef != null)
//                FirebaseFirestore.getInstance().document(sellerRef) else null,
//            defaultImgUri = defaultImgUri,
//            imgUris = if(imgUris != null) imgUris.clone() as ArrayList<String> else null,
//            rates = rates,
//            hasStock = hasStock,
//            tags = if (tags != null) tags.clone() as ArrayList<String> else null
//        )
//    }
//}