package com.blueentity.skycross.data.dao.locations

import com.blueentity.skycross.data.model.locations.Pharmacy
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class PharmacyDao(@Exclude
                       @DocumentId
                       val id: String? = null,
                       val name: String? = null,
                       val logoUri: String? = null,
                       val hotline: String? = null) {

    fun toPharmacy(): Pharmacy {
        return Pharmacy(
            id = id!!,
            name = name!!,
            logoUri = logoUri!!,
            hotline = hotline!!
        )
    }
}