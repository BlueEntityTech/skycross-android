package com.blueentity.skycross.data.model

import com.google.firebase.firestore.DocumentReference

sealed class Product {
    data class NonDrug(val id: String,
                       val name: String,
                       val desc: String,
                       val packaging: String,
                       val manufacturer: String,
                       val quantity: Double,
                       val unit: String,
                       val tags: ArrayList<String>) : Product()

    data class Branded(val id: String,
                       val name: String,
                       val desc: String,
                       val dosage: String,
                       val generic: Generic? = null,
                       val genericRef: DocumentReference,
                       val packaging: String,
                       val quantity: Double,
                       val unit: String) : Product() {
        fun withGeneric(generic: Generic) : Branded {
            return copy(generic = generic)
        }
    }
}
