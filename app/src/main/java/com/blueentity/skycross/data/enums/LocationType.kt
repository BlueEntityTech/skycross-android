package com.blueentity.skycross.data.enums

enum class LocationType {
    BRANCH,
    PHARMACY,
    HOSPITAL
}
