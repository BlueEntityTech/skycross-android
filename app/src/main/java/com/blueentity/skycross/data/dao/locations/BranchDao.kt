package com.blueentity.skycross.data.dao.locations

import com.blueentity.skycross.data.model.locations.Branch
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.GeoPoint

data class BranchDao(@Exclude
                     @DocumentId
                     val id: String? = null,
                     val name: String? = null,
                     val address: Map<String, String>? = null,
                     val location: GeoPoint? = null,
                     val imageUri: String? = null,
                     @field:JvmField
                     val isOpen: Boolean? = null,
                     val pharmacyRef: DocumentReference? = null,
                     val workHours: ArrayList<Map<String, Int>>? = null) {

    fun toBranch() : Branch {
        return Branch(
            id = id!!,
            name = name!!,
            address = address!!,
            location = location!!,
            imageUri = imageUri!!,
            isOpen = isOpen!!,
            pharmacyRef = pharmacyRef!!,
            workHours = workHours!!
        )
    }
}