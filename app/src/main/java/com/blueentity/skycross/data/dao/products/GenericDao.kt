package com.blueentity.skycross.data.dao.products

import com.blueentity.skycross.data.model.Generic
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class GenericDao(@Exclude
                      @DocumentId
                      val id: String? = null,
                      val name: String? = null,
                      val desc: String? = null) {

    fun toProduct(): Generic {
        return Generic(
            id = id!!,
            name = name!!,
            desc = desc!!
        )
    }
}