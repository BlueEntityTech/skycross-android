package com.blueentity.skycross.data.model

data class Generic(val id: String,
                   val name: String,
                   val desc: String)