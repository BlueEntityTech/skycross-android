package com.blueentity.skycross.data.model.locations

data class Pharmacy(val id: String,
                    val name: String,
                    val logoUri: String,
                    val hotline: String)