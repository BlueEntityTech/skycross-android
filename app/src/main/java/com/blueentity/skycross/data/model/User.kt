package com.blueentity.skycross.data.model

import com.blueentity.skycross.data.dao.products.ItemDao

data class User(val id: String,
                val email: String,
                val firstName: String,
                val lastName: String,
                val items: ArrayList<ItemDao>,
                val imageUri: String? = null)