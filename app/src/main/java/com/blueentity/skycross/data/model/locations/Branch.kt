package com.blueentity.skycross.data.model.locations

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.GeoPoint

data class Branch(val id: String,
                  val name: String,
                  val address: Map<String, String>,
                  val location: GeoPoint,
                  val imageUri: String,
                  val isOpen: Boolean,
                  val pharmacy: Pharmacy? = null,
                  val pharmacyRef: DocumentReference,
                  val workHours: ArrayList<Map<String, Int>>) {

    fun withPharmacy(pharmacy: Pharmacy): Branch {
        return copy(pharmacy = pharmacy)
    }
}