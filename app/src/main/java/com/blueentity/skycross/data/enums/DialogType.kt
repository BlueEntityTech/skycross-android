package com.blueentity.skycross.data.enums

enum class DialogType {
    LISTITEM,
    BROWSE
}
