package com.blueentity.skycross.data.dao.products

import com.blueentity.skycross.data.model.Product
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties

@IgnoreExtraProperties
data class ProductDao(@Exclude
                      @DocumentId
                      val id: String? = null,
                      val name: String? = null,
                      val desc: String? = null,
                      val manufacturer: String? = null,
                      val packaging: String? = null,
                      val quantity: Double? = null,
                      val unit: String? = null,
                      val tags: ArrayList<String>? = null) {

    fun toProduct() : Product.NonDrug {
        return Product.NonDrug(
            id = id!!,
            name = name!!,
            desc = desc!!,
            packaging = packaging!!,
            manufacturer = manufacturer!!,
            quantity = quantity!!,
            unit = unit!!,
            tags = tags!!
        )
    }
}