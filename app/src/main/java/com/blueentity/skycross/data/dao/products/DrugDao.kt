package com.blueentity.skycross.data.dao.products

import com.blueentity.skycross.data.model.Product
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Exclude

data class DrugDao(@Exclude
                   @DocumentId
                   val id: String? = null,
                   val name: String? = null,
                   val desc: String? = null,
                   val dosage: String? = null,
                   val genericRef: DocumentReference? = null,
                   val packaging: String? = null,
                   val quantity: Double? = null,
                   val unit: String? = null) {

    fun toProduct() : Product.Branded {
        return Product.Branded(
            id = id!!,
            name = name!!,
            desc = desc!!,
            dosage = dosage!!,
            genericRef = genericRef!!,
            packaging = packaging!!,
            quantity = quantity!!,
            unit = unit!!
        )
    }
}